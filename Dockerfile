FROM squidfunk/mkdocs-material as builder
RUN pip install mkdocs-git-revision-date-plugin
COPY .git /docs/.git
COPY docs /docs/docs
COPY overrides /docs/overrides
COPY mkdocs.yml /docs/
RUN mkdocs build

FROM nginx:stable-alpine
COPY --from=builder /docs/site /usr/share/nginx/html/docs
EXPOSE 80
