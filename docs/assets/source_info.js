function renderSourceInfo(info) {
  if (!info) {
    return undefined;
  }

  const list = document.createElement("ul");
  list.classList.add("md-source__facts");
  const version = document.createElement("li");
  version.classList.add("md-source__fact", "md-source__fact--version");
  version.innerText = info.name;
  list.appendChild(version)
  return list;
}

async function fetchSourceInfo() {
  try {
    const response = await fetch("https://git.kuschku.de/api/v4/projects/7/repository/tags?per_page=1")
    const data = await response.json();
    return data[0];
  } catch (_) {
    return undefined;
  }
}

async function updateSource() {
  const target = document.querySelector(".md-source__repository");
  const info = await fetchSourceInfo();
  const element = renderSourceInfo(info);
  target?.appendChild(element);
}

document.addEventListener("DOMContentLoaded", updateSource);
