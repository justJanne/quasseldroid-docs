---
title: Overview
description: Documentation for Quasseldroid
---

Welcome to the documentation for Quasseldroid!

!!! warning
    This documentation is still being worked on. Most of it is already ready,
    but a lot is still missing.

This documentation is split into two parts — the [Usage] and [Developer] 
sections. Additionally, [Javadocs] are available for libquassel.

[Usage]: usage/faq.md
[Developer]: protocol/overview.md
[Javadocs]: https://quasseldroid.info/javadoc/
