---
title: Features
description: Developer documentation for optional features of the Quassel protocol
---

Legacy features encode the flags of each supported feature, extended features encodes a QStringList of the names of all
supported features.

If no flag is given, this feature is not included when legacy features are encoded.

| Flag         | Name                   | Description                                                            |
|--------------|------------------------|------------------------------------------------------------------------|
| `0x00000001` | SynchronizedMarkerLine | --                                                                     |
| `0x00000002` | SaslAuthentication     | --                                                                     |
| `0x00000004` | SaslExternal           | --                                                                     |
| `0x00000008` | HideInactiveNetworks   | --                                                                     |
| `0x00000010` | PasswordChange         | --                                                                     |
| `0x00000020` | CapNegotiation         | IRCv3 capability negotiation, account tracking                         |
| `0x00000040` | VerifyServerSSL        | IRC server SSL validation                                              |
| `0x00000080` | CustomRateLimits       | IRC server custom message rate limits                                  |
| `0x00000100` | DccFileTransfer        | Currently not supported                                                |
| `0x00000200` | AwayFormatTimestamp    | Timestamp formatting in away (e.g. %%hh:mm%%)                          |
| `0x00000400` | Authenticators         | Support for exchangeable auth backends                                 |
| `0x00000800` | BufferActivitySync     | Sync buffer activity status                                            |
| `0x00001000` | CoreSideHighlights     | Core-Side [HighlightRuleManager] and matching                          |
| `0x00002000` | SenderPrefixes         | Show prefixes for senders in backlog                                   |
| `0x00004000` | RemoteDisconnect       | Supports RPC call [disconnectFromCore] to remotely disconnect a client |
| `0x00008000` | ExtendedFeatures       | Transmit features as list of strings                                   |
| --           | LongTime               | Serialize message time as 64-bit                                       |
| --           | RichMessages           | Real Name and Avatar URL in backlog                                    |
| --           | BacklogFilterType      | [BacklogManager] supports filtering backlog by [MessageType]           |
| --           | EcdsaCertfpKeys        | ECDSA keys for CertFP in [CertManager]                                 |
| --           | LongMessageId          | 64-bit [MsgId] for [Message]s                                          |
| --           | SyncedCoreInfo         | [CoreInfo] dynamically updated using signals                           |

[HighlightRuleManager]: ../signalproxy/objects.md#highlightrulemanager
[disconnectFromCore]: ../signalproxy/rpc.md#2disconnectfromcore
[BacklogManager]: ../signalproxy/objects.md#backlogmanager
[MessageType]: ../serialization.md#messagetype
[CertManager]: ../signalproxy/objects.md#certmanager
[MsgId]: ../serialization.md#type-aliases
[Message]: ../serialization.md#message
[CoreInfo]: ../signalproxy/objects.md#coreinfo
