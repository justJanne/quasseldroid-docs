---
title: Handshake Messages
description: Developer documentation for messages exchanged during the handshake of the Quassel protocol
---

This document discusses how complex objects are translated to and from QVariantMaps.

Handshake Types are (de-)serialized to and from QVariantMaps. An additional key/value pair "MsgType" is added, which
contains the name of the complexStruct’s type – for example `ClientInitAck` – as QString.

For en/decoding the specified fields see [Serialization].

## ClientInit

| Field          | Type          | Description                         |
|----------------|---------------|-------------------------------------|
| ClientVersion  | `QString`     | Version of the client               |
| BuildDate      | `QString`     | Build date of the client            | 
| ClientFeatures | `UInt`        | Flags of supported legacy features  |
| FeatureList    | `QStringList` | List of supported extended features |

## ClientInitReject

| Field       | Type    | Description                                     |
|-------------|---------|-------------------------------------------------|
| ErrorString | QString | String with an error message of what went wrong |

## ClientInitAck

| Field             | Type           | Description                                              |
|-------------------|----------------|----------------------------------------------------------|
| CoreFeatures      | `UInt`         | Flags of supported legacy features                       |
| CoreConfigured    | `Bool`         | If the core has already been configured                  |
| BackendInfo       | `QVariantList` | List of QVariantMaps of info on available backends       |
| AuthenticatorInfo | `QVariantList` | List of QVariantMaps of info on available authenticators |
| FeatureList       | `QStringList`  | List of supported extended features                      |

The BackendInfo/AuthenticatorInfo lists contain a list of available backends/authenticators, and for each one a list of
all parameters with types and default values.

## CoreSetupData

| Field         | Type          | Description                    |
|---------------|---------------|--------------------------------|
| AdminUser     | `QString`     | Username for the first user    |
| AdminPassword | `QString`     | Password for the first user    |
| Backend       | `QString`     | Name of selected backend       |
| SetupData     | `QVariantMap` | Backend setup parameters       |
| Authenticator | `QString`     | Name of selected authenticator |
| AuthSetupData | `QVariantMap` | Authenticator setup parameters | 

## CoreSetupReject

| Field       | Type    | Description                                     |
|-------------|---------|-------------------------------------------------|
| ErrorString | QString | String with an error message of what went wrong |

## CoreSetupAck

This message has no fields

## ClientLogin

| Field    | Type      | Description            |
|----------|-----------|------------------------|
| User     | `QString` | Username for login     |
| Password | `QString` | Corresponding password |

## ClientLoginReject

| Field       | Type    | Description                                     |
|-------------|---------|-------------------------------------------------|
| ErrorString | QString | String with an error message of what went wrong |

## ClientLoginAck

This message has no fields

## SessionInit

| Field       | Type           | Description                       |
|-------------|----------------|-----------------------------------|
| Identities  | `QVariantList` | List of all configured identities |
| BufferInfos | `QVariantList` | List of all existing buffers      |
| NetworkIds  | `QVariantList` | Ids of all networks               |

[Serialization]: ../serialization.md
