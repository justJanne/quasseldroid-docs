---
title: Message Layer
description: Developer documentation of the message layer of the Quassel Protocol
---

This describes the second layer of the protocol, the message layer, which rests upon the [Framing Layer].

## State Machine

The message layer has two possible states: `HANDSHAKE` and `CONNECTED`. It starts off in `HANDSHAKE` state.

## Handshake State

In this state, messages are encoded/decoded according to the
[Handshake Messages] specification.

[![Init Diagram](../../images/message_init.png)](../../images/message_init.png)

Client and core exchange messages according to the diagram described above, when a ClientLoginAck and SessionInit have
been received, the state advances into
`CONNECTED`.

### Core Setup

[![Core Setup Diagram](../../images/message_setup.png)](../../images/message_setup.png)

When the client receives a CoreInitAck message, with "Configured" set to
"False", it will display to the user a list, allowing them to choose a StorageBackend from the ones present in
CoreInitAck’s "StorageBackends"
attribute.

If the user chooses one such backend, and the backend has a SetupKeys attribute which is not-empty, the user will be
presented with input fields for each of the keys, pre-populated with the values of the SetupDefaults map. Each field
allows only inputs of the same type as the default value, so, for example, a field named "Port" which has the default
value QVariant(Int, 4242) would only allow integers to be input.

As soon as the user has properly filled out and configured these settings, they will choose the username and password of
the new account that is to be set up on the core.

The first account set up is a so-called "Admin Account", although this does not yet provide any special abilities.

The client will then send a new CoreSetupData message containing a SetupData attribute with all the previously specified
settings.

If the client receives a CoreSetupReject, it will display the specified error to the user, and, if wished, allow them to
try configuring again.

If the client receives a CoreSetupAck, the client will proceed normally in the init procedure, by sending the
ClientLogin message.

## Connected State

In this state, messages are serialized according to the [SignalProxy Messages]
specification.

[Framing Layer]: framing.md
[Handshake Messages]: ../handshake/messages.md
[SignalProxy Messages]: ../signalproxy/messages.md
