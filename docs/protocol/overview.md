---
title: Protocol Overview
description: Overview over the Quassel Protocol
---

Our protocol is a multi-layered protocol.

As bottom most layer we have the [Framing Layer]. This does the most basic feature negotiation, compression/encryption,
and allows splitting the stream into separate message frames.

On top of that rests the [Message Layer]. It handles handshaking, core setup, login, and afterwards provides the ability
to request objects, initialize them, and run sync/rpc calls on them.

For our serialization, we describe the basic most functionality in [Serialization], and for the different layers on top,
in [Handshake Messages] and [SignalProxy Messages].

Features for the Message Layer level feature negotiation are specified in [Features].

The sync calls, the sync object serialization, and rpc calls are specified in [SignalProxy Objects]
and [SignalProxy RPC Calls].

Our business logic is based solely on these sync, init, and rpc calls. It will be described at a later time.

[Serialization]: serialization.md
[Framing Layer]: layers/framing.md
[Message Layer]: layers/message.md
[Features]: handshake/features.md
[Handshake Messages]: handshake/messages.md
[SignalProxy Messages]: signalproxy/messages.md
[SignalProxy Objects]: signalproxy/objects.md
[SignalProxy RPC Calls]: signalproxy/rpc.md
