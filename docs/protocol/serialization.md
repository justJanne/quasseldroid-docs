---
title: Serialization
description: Developer documentation for on-wire representation of types used in Quassel protocol messages
---

All serialization is done in network format big-endian.

Serialization of basic types is trivial and will not be discussed here.

Primitive types are serialized by serializing each of their fields sequentially.

## Type Aliases

Certain types have a representation which is identical to a pre-existing type in this document. For this reason, we do
not define them twice, but list the type whose network representation they borrow here:

| Type name       | Network representation |
|-----------------|------------------------|
| BufferId        | `Int`                  |
| IdentityId      | `Int`                  |
| NetworkId       | `Int`                  |
| MsgId           | `Int`                  |
| PeerPtr         | `Long`                 |
| IrcUser         | `QVariantMap`          |
| IrcChannel      | `QVariantMap`          |
| Identity        | `QVariantMap`          |
| NetworkInfo     | `QVariantMap`          |
| Network::Server | `QVariantMap`          |

MsgId’s serialization changes depending on which features are available on both client and server. Specifically,
if `LongMessageId` is available, it is serialized as long, and its values can go above INT_MAX.

## String

Strings are serialized as a signed int of the length in bytes plus their content in UTF-16BE. If the length is -1, the
String is to be considered equivalent to
`NULL`.

## StringList

StringLists are serialized as an `Int` of the amount of elements, and a
`string` for each element.

## Variant

First, an `Int` for the type, then an `UChar` for an unknown value, then, if the type is a usertype, the typename
as `bytearray` (C-string with null byte at the end), and then the value in its own format.

### List

VariantLists are serialized as an `Int` of the length, and a `Variant` for each element.

### Map

VariantMaps are serialized as an `Int` of the amount of keys, and then for each key a `QString` for the key and
a `Variant` for each element.

### Type

| Enum         | Value        | Additional Info |
|--------------|--------------|-----------------|
| Void         | `0x00000000` |                 |
| Bool         | `0x00000001` |                 |
| Int          | `0x00000002` | `int32_t`       |
| UInt         | `0x00000003` | `uint32_t`      |
| QChar        | `0x00000007` |                 |
| QVariantMap  | `0x00000008` |                 |
| QVariantList | `0x00000009` |                 |
| QString      | `0x0000000a` |                 |
| QStringList  | `0x0000000b` |                 |
| QByteArray   | `0x0000000c` |                 |
| QTime        | `0x0000000f` |                 |
| QDateTime    | `0x00000010` |                 |
| UserType     | `0x0000007f` |                 |
| Long         | `0x00000081` | `int64_t`       |
| Short        | `0x00000082` | `int16_t`       |
| Char         | `0x00000083` | `int8_t`        |
| ULong        | `0x00000084` | `uint64_t`      |
| UShort       | `0x00000085` | `uint16_t`      |
| UChar        | `0x00000086` | `uint8_t`       |
| QVariant     | `0x00000090` |                 |

## DateTime

| Type    | Field       | Description                                           |
|---------|-------------|-------------------------------------------------------|
| `Int`   | julianDay   | Day in Julian calendar, unknown if signed or unsigned |
| `Int`   | millisOfDay | Milliseconds since start of day                       |
| `UChar` | zone        | Timezone of DateTime, `0x00` is local, `0x01` is UTC  |

## Time

| Type  | Field       | Description                     |
|-------|-------------|---------------------------------|
| `Int` | millisOfDay | Milliseconds since start of day |

## BufferInfo

| Type         | Field     | Description                                      |
|--------------|-----------|--------------------------------------------------|
| `Int`        | id        | BufferId, a unique, sequential id for the buffer |
| `Int`        | networkId | NetworkId of the network the buffer belongs to   |
| `Short`      | type      | See below                                        |
| `QByteArray` | name      | (UTF-8) BufferName as displayed to the user      |

### BufferType

| Enum    | Value  |
|---------|--------|
| Status  | `0x01` |
| Channel | `0x02` |
| Query   | `0x04` |
| Group   | `0x08` |

## Message

| Type         | Field          | Description                                                                                          |
|--------------|----------------|------------------------------------------------------------------------------------------------------|
| `MsgId`      | messageId      | The unique, sequential id for the message                                                            |
| `Int`        | timestamp      | The timestamp of the message in UNIX time (32-bit, seconds)                                          |
| `Long`       |                | **CONDITIONAL**: Different Type (Long) if LongTime feature is enabled                                |
| `Int`        | type           | See below                                                                                            |
| `UChar`      | flags          | See below                                                                                            |
| `BufferInfo` | buffer         | The buffer the message belongs to, usually everything but BufferId is set to `NULL`                  |
| `QByteArray` | sender         | (UTF-8) The sender as `nick!ident@host`                                                              |
| `QByteArray` | senderPrefixes | (UTF-8) The prefix modes of the sender                                                               |
|              |                | **CONDITIONAL**: Only serialized if SenderPrefixes feature is enabled                                |
| `QByteArray` | realName       | (UTF-8) The realName of the sender                                                                   |
|              |                | **CONDITIONAL**: Only serialized if RichMessages feature is enabled                                  |
| `QByteArray` | avatarUrl      | (UTF-8) The avatarUrl of the sender, if available                                                    |
|              |                | **CONDITIONAL**: Only serialized if RichMessages feature is enabled                                  |
| `QByteArray` | content        | (UTF-8) The message content, already stripped from CTCP formatting, but containing mIRC format codes |

This type's serialization format depends heavily on the available features — if both client and server support a certain
feature, additional fields are added or changed.

| Field          | Feature        | Effect                                                                  |
|----------------|----------------|-------------------------------------------------------------------------|
| timestamp      | LongTime       | Type is Long instead of Int, content is milliseconds instead of seconds |
| senderPrefixes | SenderPrefixes | Field is serialized only with feature present                           |
| realName       | RichMessages   | Field is serialized only with feature present                           |
| avatarUrl      | RichMessages   | Field is serialized only with feature present                           |

### MessageType

| Enum         | Value        |
|--------------|--------------|
| Plain        | `0x00000001` |
| Notice       | `0x00000002` |
| Action       | `0x00000004` |
| Nick         | `0x00000008` |
| Mode         | `0x00000010` |
| Join         | `0x00000020` |
| Part         | `0x00000040` |
| Quit         | `0x00000080` |
| Kick         | `0x00000100` |
| Kill         | `0x00000200` |
| Server       | `0x00000400` |
| Info         | `0x00000800` |
| Error        | `0x00001000` |
| DayChange    | `0x00002000` |
| Topic        | `0x00004000` |
| NetsplitJoin | `0x00008000` |
| NetsplitQuit | `0x00010000` |
| Invite       | `0x00020000` |

### MessageFlags

| Type       | Value        |
|------------|--------------|
| Self       | `0x00000001` |
| Highlight  | `0x00000002` |
| Redirected | `0x00000004` |
| ServerMsg  | `0x00000008` |
| Backlog    | `0x00000080` |
