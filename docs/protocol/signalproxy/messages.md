---
title: Messages
description: Developer documentation for messages of the Quassel protocol
---

Sync messages are (de-)serialized to and from QVariantLists. They are encoded as the request type, followed by the
parameters of that type.

For en/decoding the specified fields see [Serialization].

## Request Type Enum

| Value        | Name           | Direction     |
|--------------|----------------|---------------|
| `0x00000001` | SyncMessage    | bidirectional |
| `0x00000002` | RpcCall        | bidirectional |
| `0x00000003` | InitRequest    | C->S          |
| `0x00000004` | InitData       | S->C          |
| `0x00000005` | HeartBeat      | bidirectional |
| `0x00000006` | HeartBeatReply | bidirectional |

## SyncMessage

| Parameter  | Type           | Description                                                                     |
|------------|----------------|---------------------------------------------------------------------------------|
| ClassName  | `QByteArray`   | Encoded as UTF-8                                                                |
| ObjectName | `QByteArray`   | Encoded as UTF-8                                                                |
| SlotName   | `QByteArray`   | Encoded as UTF-8                                                                |
| Params     | `QVariantList` | Not an actual parameter, just the remaining elements of the list (may be empty) |

## RpcCall

| Parameter | Type           | Description                                                                     |
|-----------|----------------|---------------------------------------------------------------------------------|
| SlotName  | `QByteArray`   | Encoded as UTF-8                                                                |
| Params    | `QVariantList` | Not an actual parameter, just the remaining elements of the list (may be empty) |

## InitRequest

| Parameter  | Type         | Description      |
|------------|--------------|------------------|
| ClassName  | `QByteArray` | Encoded as UTF-8 |
| ObjectName | `QByteArray` | Encoded as UTF-8 |

## InitData

| Parameter  | Type          | Description      |
|------------|---------------|------------------|
| ClassName  | `QByteArray`  | Encoded as UTF-8 |
| ObjectName | `QByteArray`  | Encoded as UTF-8 |
| InitData   | `QVariantMap` | See below        |

The InitData field is not an actual parameter, just the remaining elements of the list (maybe empty), grouped into pairs
of 2, interpreted as key and value respectively.

These are according to the [SignalProxy Objects] specification.

## HeartBeat

| Parameter | Name        |
|-----------|-------------|
| Timestamp | `QDateTime` |

In case this is received, the other side expects a corresponding HeartBeatReply with the same timestamp.

## HeartBeatReply

| Parameter | Name        |
|-----------|-------------|
| Timestamp | `QDateTime` |

[Serialization]: ../serialization.md
[SignalProxy Objects]: objects.md
