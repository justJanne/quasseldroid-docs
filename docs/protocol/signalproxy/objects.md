---
title: Objects
description: Developer documentation for SignalProxy objects of the Quassel protocol
---

SignalProxy objects are special, because these are actual classes you’ll use in your representation of the client state,
with actual behavior (described in another document).

All of these objects are translated into `QVariantMap` as network representation, this document describes their fields
at runtime, and how those are translated into the maps used in `InitData` responses and arguments to `Sync` and `RPC`
messages.

All types in this document are written in typescript typings notation.

In Network representation, all map/hash notations refer to QVariantMaps, all list/array notations to QVariantList.

## AoS to SoA

For most network objects, the runtime representation contains a "Array of Structures" (AoS), while, for better
compression, on the network a "Structure of Arrays" (SoA) representation is used.

As this protocol uses QVariantMap with QString keys in UTF-16BE, this saves significant overhead.

This means a runtime representation of

```json
[
        { "name": "intro", "expansion": "/welcome $1; /assist" },
        { "name": "welcome", "expansion": "/say Welcome to the support channel for the IRC client Quassel, $1"},
        { "name": "assist", "expansion": "/say How may I assist you today?" }
]
```

is translated into

```json
{
        "names": [ "intro", "welcome", "assist" ],
        "expansions": [ "/welcome $1; /assist", "/say Welcome to the support channel for the IRC client Quassel, $1", "/say How may I assist you today?" ]
}
```

## QVariantMap to QVariantList

This translation is also common, it means that a QVariantMap is serialized as QVariantList with keys and values
interspersed.

This means a runtime representation of

```json
{ "name": "intro", "expansion": "/welcome $1; /assist" }
```

is translated into

```json
[ "name", "intro", "expansion", "/welcome $1; /assist" ]
```

## AliasManager

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface AliasManager {
    aliases: Alias[]


    // C->S calls

    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    requestUpdate(properties: QVariantMap)


    // S->C calls

    addAlias(name: QString, expansion: QString)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}

interface Alias {
    name: QString,
    expansion: QString
}
```

**Network**

Applied translations:

- [AoS to SoA]

```typescript
interface AliasManager {
    Aliases: {
        names: QStringList,
        expansions: QStringList
    }
}
```

## BacklogManager

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface BacklogManager {


    // C->S calls

    /**
     * Loads backlog for [bufferId], where the message id is >= [first] and < [last].
     * If [first] or [last] is unset, the list will be unbounded in that direction.
     *
     * If a [limit] is set, the list will be truncated to the newest N messages.
     *
     * If both [first] and [last] are set, and the list of messages is not truncated by [limit],
     * [additional] messages will be loaded before [last].
     */
    requestBacklog(bufferId: BufferId, first: MsgId, last: MsgId, limit: Int, additional: Int)
    /**
     * Loads backlog for [bufferId], where the message id is >= [first] and < [last].
     * If [first] or [last] is unset, the list will be unbounded in that direction.
     *
     * If a [limit] is set, the list will be truncated to the newest N messages.
     *
     * If both [first] and [last] are set, and the list of messages is not truncated by [limit],
     * [additional] messages will be loaded before [last].
     *
     * Only messages matching [type] and [flags] will be returned and counted.
     */
    requestBacklogFiltered(bufferId: BufferId, first: MsgId, last: MsgId, limit: Int, additional: Int, type: Int, flags: Int)
    /**
     * Loads backlog for [bufferId], where the message id is >= [first] and < [last].
     * If [first] or [last] is unset, the list will be unbounded in that direction.
     *
     * If a [limit] is set, the list will be truncated to the oldest N messages.
     *
     * Only messages matching [type] and [flags] will be returned and counted.
     */
    requestBacklogForward(bufferId: BufferId, first: MsgId, last: MsgId, limit: Int, type: Int, flags: Int)
    /**
     * Loads backlog for all buffers, where the message id is >= [first] and < [last].
     * If [first] or [last] is unset, the list will be unbounded in that direction.
     *
     * If a [limit] is set, the list will be truncated to the newest N messages.
     *
     * If both [first] and [last] are set, and the list of messages is not truncated by [limit],
     * [additional] messages will be loaded before [last].
     */
    requestBacklogAll(first: MsgId, last: MsgId, limit: Int, additional: Int)
    /**
     * Loads backlog for all buffers, where the message id is >= [first] and < [last].
     * If [first] or [last] is unset, the list will be unbounded in that direction.
     *
     * If a [limit] is set, the list will be truncated to the newest N messages.
     *
     * If both [first] and [last] are set, and the list of messages is not truncated by [limit],
     * [additional] messages will be loaded before [last].
     *
     * Only messages matching [type] and [flags] will be returned and counted.
     */
    requestBacklogAllFiltered(first: MsgId, last: MsgId, limit: Int, additional: Int, type: Int, flags: Int)


    // S->C calls

    /**
     * Response to the corresponding [requestBacklog] call.
     * [messages] contains the messages as `QVariant<Message>`
     */
    receiveBacklog(bufferId: BufferId, first: MsgId, last: MsgId, limit: Int, additional: Int, messages: QVariantList)
    /**
     * Response to the corresponding [requestBacklogFiltered] call.
     * [messages] contains the messages as `QVariant<Message>`
     */
    receiveBacklogFiltered(bufferId: BufferId, first: MsgId, last: MsgId, limit: Int, additional: Int, type: Int, flags: Int, messages: QVariantList)
    /**
     * Response to the corresponding [requestBacklogForward] call.
     * [messages] contains the messages as `QVariant<Message>`
     */
    receiveBacklogForward(bufferId: BufferId, first: MsgId, last: MsgId, limit: Int, type: Int, flags: Int, messages: QVariantList)
    /**
     * Response to the corresponding [requestBacklogAll] call.
     * [messages] contains the messages as `QVariant<Message>`
     */
    receiveBacklogAll(first: MsgId, last: MsgId, limit: Int, additional: Int, messages: QVariantList)
    /**
     * Response to the corresponding [requestBacklogAllFiltered] call.
     * [messages] contains the messages as `QVariant<Message>`
     */
    receiveBacklogAllFiltered(first: MsgId, last: MsgId, limit: Int, additional: Int, type: Int, flags: Int, messages: QVariantList)
}
```

**Network**

Its network representation is an empty `QVariantMap`, as it has no syncable fields. For the same reason it also does
**not** support the otherwise standard `update` call.

## BufferSyncer

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface BufferSyncer {
    /**
     * A bitset of all message types of unread messages for each chat.
     */
    activities: { [key:BufferId]: Message.Type },
    /**
     * Number of unread messages with highlights per chat.
     */
    highlightCounts: { [key:BufferId]: Int },
    /**
     * The last "marked as read" message per chat
     */
    lastSeenMsg: { [key:BufferId]: MsgId },
    /**
     * The scrollposition at the bottom of the window of the last client that had
     * each chat open
     */
    markerLine: { [key:BufferId]: MsgId }


    // C->S calls

    requestMarkBufferAsRead(buffer: BufferId)
    requestMergeBuffersPermanently(buffer1: BufferId, buffer2: BufferId)
    requestPurgeBufferIds()
    requestRemoveBuffer(buffer: BufferId)
    requestRenameBuffer(buffer: BufferId)
    requestSetLastSeenMsg(buffer: BufferId, msgid: MsgId)
    requestSetMarkerLine(buffer: BufferId, msgid: MsgId)


    // S->C calls

    markBufferAsRead(buffer: BufferId)
    mergeBuffersPermanently(buffer1: BufferId, buffer2: BufferId)
    removeBuffer(buffer: BufferId)
    renameBuffer(buffer: BufferId, newName: QString)
    setBufferActivity(buffer: BufferId, activity: Int)
    setHighlightCount(buffer: BufferId, count: Int)
    setLastSeenMsg(buffer: BufferId, msgId: MsgId)
    setMarkerLine(buffer: BufferId, msgId: MsgId)
}
```

**Network**

Applied translations:

- [QVariantMap to QVariantList]

```typescript
interface BufferSyncer {
    Activities: { [key:BufferId]: Int },
    HighlightCounts: { [key:BufferId]: Int },
    LastSeenMsg: { [key:BufferId]: MsgId },
    MarkerLines: { [key:BufferId]: MsgId }
}
```

## BufferViewConfig

**ObjectName**

The objectName of a BufferViewConfig is the string representation of the bufferViewId.

Example: `0`

**Runtime**

```typescript
interface BufferViewConfig {
    buffers: BufferId[],
    removedBuffers: BufferId[],
    temporarilyRemovedBuffers: BufferId[],

    /** ID of the associated BufferView */
    bufferViewId: Int,
    /** Display name of the associated BufferView */
    bufferViewName: QString,
    /** If showing only buffers from one network, the NetworkId of it */
    networkId: NetworkId,
    /** Automatically add new buffers when created */
    addNewBuffersAutomatically: Bool,
    /** Sort buffers alphabetically */
    sortAlphabetically: Bool,
    /** Hide buffers which are disconnected/parted */
    hideInactiveBuffers: Bool,
    /** Hide networks which are disconnected/quit */
    hideInactiveNetworks: Bool,
    /** Disable buffer decoration (not fully implemented) */
    disableDecoration: Bool,
    /**
     * Filter buffers by type
     * Int value of the BufferInfo.Type enum
     */
    allowedBuffersTypes: Int,
    /**
     * Filter buffers by minimum activity
     * Bitset of the BufferInfo.Activity enum values which should be shown
     */
    minimumActivity: Int,
    /** Persistently show the buffer search UI */
    showSearch: Bool


    // C->S calls

    requestAddBuffer(bufferId: BufferId, pos: Int)
    requestMoveBuffer(bufferId: BufferId, pos: Int)
    requestRemoveBuffer(bufferId: BufferId)
    requestRemoveBufferPermanently(bufferId: BufferId)
    requestSetBufferViewName(bufferViewName: QString)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    requestUpdate(properties: QVariantMap)


    // S->C calls
    addBuffer(bufferId: BufferId, pos: Int)
    moveBuffer(bufferId: BufferId, pos: Int)
    removeBuffer(bufferId: BufferId)
    removeBufferPermanently(bufferId: BufferId)
    setAddNewBuffersAutomatically(addNewBuffersAutomatically: Bool)
    setAllowedBufferTypes(bufferTypes: Int)
    setBufferViewName(bufferViewName: QString)
    setDisableDecoration(disableDecoration: Bool)
    setHideInactiveBuffers(hideInactiveBuffers: Bool)
    setHideInactiveNetworks(hideInactiveNetworks: Bool)
    setMinimumActivity(activity: Int)
    setNetworkId(networkId: NetworkId)
    setShowSearch(showSearch: Bool)
    setSortAlphabetically(sortAlphabetically: Bool)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}
```

**Network**

```typescript
interface BufferViewConfig {
    BufferList: BufferId[],
    RemovedBuffers: BufferId[],
    TemporarilyRemovedBuffers: BufferId[],

    bufferViewName: QString,
    networkId: NetworkId,
    addNewBuffersAutomatically: Bool,
    sortAlphabetically: Bool,
    hideInactiveBuffers: Bool,
    hideInactiveNetworks: Bool,
    disableDecoration: Bool,
    allowedBuffersTypes: Int,
    minimumActivity: Int,
    showSearch: Bool
}
```

## BufferViewManager

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface BufferViewManager {
    bufferViewConfigs: { [key:Int]: BufferViewConfig },


    // C->S calls

    requestCreateBufferView(properties: QVariantMap)
    requestCreateBufferViews(properties: QVariantList)
    requestDeleteBufferView(bufferViewId: Int)
    requestDeleteBufferViews(bufferViews: QVariantList)


    // S->C calls

    addBufferViewConfig(bufferViewConfigId: Int)
    deleteBufferViewConfig(bufferViewConfigId: Int)
    newBufferViewConfig(bufferViewConfigId: Int)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}
```

**Network**

```typescript
interface BufferViewManager {
    BufferViewIds: Int[]
}
```

## CertManager

**ObjectName**

The objectName of a CertManager is the string representation of the identityId of the Identity it belongs to.

Example: `2`

**Runtime**

```typescript
interface CertManager {
    sslKey: QByteArray,
    sslCert: QByteArray


    // C->S calls

    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    requestUpdate(properties: QVariantMap)


    // S->C calls

    setSslCert(encoded: QByteBuffer | null)
    setSslKey(encoded: QByteBuffer | null)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}
```

**Network**

```typescript
interface CertManager {
    sslKey: QByteArray,
    sslCert: QByteArray
}
```

## CoreInfo

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface CoreInfo {
    coreData: CoreData

    // S->C calls
    setCoreData(coreData: QVariantMap)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}

interface CoreData {
    quasselVersion: QString,
    quasselBuildDate: QString,
    startTime: QDateTime,
    sessionConnectedClients: Int,
    sessionConnectedClientData: ConnectedClient[]
}

interface ConnectedClient {
    id: Int,
    remoteAddress: String,
    location: String,
    clientVersion: String,
    clientVersionDate: String,
    connectedSince: QDateTime,
    secure: Bool,
    features: Int,
    featureList: QStringList
}
```

**Network**

```typescript
interface CoreInfo {
    coreData: {
        quasselVersion: QString,
        quasselBuildDate: QString,
        startTime: QDateTime,
        sessionConnectedClients: Int,
        sessionConnectedClientData: ConnectedClient[]
    }
}

interface ConnectedClient {
    id: Int,
    remoteAddress: String,
    location: String,
    clientVersion: String,
    clientVersionDate: String,
    connectedSince: QDateTime,
    secure: Bool,
    features: Int,
    featureList: QStringList  
}
```

## HighlightRuleManager

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface HighlightRuleManager {
    highlightRuleList: HighlightRule[],
    highlightNick: HighlightNickType,
    nicksCaseSensitive: Bool


    // C->S calls

    /**
     * Request removal of an ignore rule based on the rule itself.
     * Use this method if you want to remove a single ignore rule
     * and get that synced with the core immediately.
     */
    requestRemoveHighlightRule(highlightRule: Int)
    /**
     * Request toggling of "isEnabled" flag of a given ignore rule.
     * Use this method if you want to toggle the "isEnabled" flag of a single ignore rule
     * and get that synced with the core immediately.
     */
    requestToggleHighlightRule(highlightRule: Int)
    /**
     * Request an HighlightRule to be added to the ignore list
     * Items added to the list with this method, get immediately synced with the core
     */
    requestAddHighlightRule(id: Int, name: QString, isRegEx: Boolean,
        isCaseSensitive: Boolean, isEnabled: Boolean, isInverse: Boolean,
        sender: QString, chanName: QString)
    requestSetHighlightNick(highlightNick: Int)
    requestSetNicksCaseSensitive(nicksCaseSensitive: Boolean)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    requestUpdate(properties: QVariantMap)


    // S->C calls

    removeHighlightRule(highlightRule: Int)
    toggleHighlightRule(highlightRule: Int)
    addHighlightRule(id: Int, name: QString, isRegEx: Boolean,
            isCaseSensitive: Boolean, isEnabled: Boolean, isInverse: Boolean,
            sender: QString, chanName: QString)
    setHighlightNick(highlightNick: Int)
    setNicksCaseSensitive(nicksCaseSensitive: Boolean)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}

interface HighlightRule {
    id: Int,
    name: QString,
    isRegEx: Bool,
    isCaseSensitive: Bool,
    isEnabled: Bool,
    isInverse: Bool,
    sender: QString,
    channel: QString
}

enum HighlightNickType {
    NoNick = 0x00,
    CurrentNick = 0x01,
    AllNicks = 0x02
}
```

**Network**

Applied translations:

- [AoS to SoA]

```typescript
interface HighlightRuleManager {
    HighlightRuleList: {
        id: Int[],
        name: QStringList,
        isRegEx: Bool[],
        isCaseSensitive: Bool[],
        isEnabled: Bool[],
        isInverse: Bool[],
        sender: QStringList,
        channel: QStringList
    },
    highlightNick: Int,
    nicksCaseSensitive: Bool
}
```

## Identity

**ObjectName**

The objectName of an Identity is the string representation of the identityId.

Example: `2`

**Runtime**

```typescript
interface Identity {
    identityId: IdentityId,
    identityName: QString,
    realName: QString,
    nicks: QStringList,
    awayNick: QString,
    awayNickEnabled: Bool,
    awayReason: QString,
    awayReasonEnabled: Bool,
    autoAwayEnabled: Bool,
    autoAwayTime: Int,
    autoAwayReason: QString,
    autoAwayReasonEnabled: Bool,
    detachAwayEnabled: Bool,
    detachAwayReason: QString,
    detachAwayReasonEnabled: Bool,
    ident: QString,
    kickReason: QString,
    partReason: QString,
    quitReason: QString


    // C->S calls

    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    requestUpdate(properties: QVariantMap)


    // S->C calls

    copyFrom(other: Identity)
    setAutoAwayEnabled(enabled: Bool)
    setAutoAwayReason(reason: QString)
    setAutoAwayReasonEnabled(enabled: Bool)
    setAutoAwayTime(time: Int)
    setAwayNick(awayNick: QString)
    setAwayNickEnabled(enabled: Bool)
    setAwayReason(awayReason: QString)
    setAwayReasonEnabled(enabled: Bool)
    setDetachAwayEnabled(enabled: Bool)
    setDetachAwayReason(reason: QString)
    setDetachAwayReasonEnabled(enabled: Bool)
    setId(id: IdentityId)
    setIdent(ident: QString)
    setIdentityName(name: QString)
    setKickReason(reason: QString)
    setNicks(nicks: QStringList)
    setPartReason(reason: QString)
    setQuitReason(reason: QString)
    setRealName(realName: QString)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}
```

**Network**

```typescript
interface Identity {
    identityId: IdentityId,
    identityName: QString,
    realName: QString,
    nicks: QStringList,
    awayNick: QString,
    awayNickEnabled: Bool,
    awayReason: QString,
    awayReasonEnabled: Bool,
    autoAwayEnabled: Bool,
    autoAwayTime: Int,
    autoAwayReason: QString,
    autoAwayReasonEnabled: Bool,
    detachAwayEnabled: Bool,
    detachAwayReason: QString,
    detachAwayReasonEnabled: Bool,
    ident: QString,
    kickReason: QString,
    partReason: QString,
    quitReason: QString
}
```

## IgnoreListManager

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface IgnoreListManager {
    ignoreList: IgnoreListItem[]


    // C->S calls

    requestAddIgnoreListItem(type: Int, ignoreRule: QString,
        isRegEx: Bool, strictness: Int, scope: Int, scopeRule: QString,
        isActive: Bool)
    requestRemoveIgnoreListItem(ignoreRule: QString)
    requestToggleIgnoreRule(ignoreRule: QString)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    requestUpdate(properties: QVariantMap)


    // S->C calls

    addIgnoreListItem(type: Int, ignoreRule: QString, isRegEx: Bool,
        strictness: Int, scope: Int, scopeRule: QString, isActive: Bool)
    removeIgnoreListItem(ignoreRule: QString)
    toggleIgnoreRule(ignoreRule: QString)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}

interface IgnoreListItem {
    type: IgnoreType,
    ignoreRule: String,
    isRegEx: Boolean,
    strictness: StrictnessType,
    scope: ScopeType,
    scopeRule: String,
    isActive: Boolean
}

enum IgnoreType {
    SenderIgnore = 0x00,
    MessageIgnore = 0x01,
    CtcpIgnore = 0x02
}

enum StrictnessType {
    UnmatchedStrictness = 0x00,
    /**
     * Dynamic ignore, ignore rule has to be applied by the client to the
     * messages it receives
     */
    SoftStrictness = 0x01,
    /** Permanent ignore, messages don't even get saved to the database */
    HardStrictness = 0x02
}

enum ScopeType {
    GlobalScope = 0x00,
    NetworkScope = 0x01,
    ChannelScope = 0x02
}
```

**Network**

Applied translations:

- [AoS to SoA]

```typescript
interface IgnoreListManager {
    IgnoreList: {
        ignoreType: Int[],
        ignoreRule: QStringList,
        isRegEx: Bool[],
        strictness: Int[],
        scope: Int[],
        scopeRule: QStringList,
        isActive: Bool[]
    }
}
```

## IrcChannel

**ObjectName**

The objectName of an IrcChannel is the string representation of the networkId of the network it belongs to, followed by
a "/", followed by the name of the channel.

Example: `4/#quassel`

**Runtime**

```typescript
interface IrcChannel {
    channelModesA: { [key:QChar]: QStringList },
    channelModesB: { [key:QChar]: QString },
    channelModesC: { [key:QChar]: QString },
    channelModesD: QChar[],
    name: QString,
    topic: QString,
    password: QString,
    encrypted: Bool


    // S->C calls

    addChannelMode(mode: QChar, value: QString)
    addUserMode(nick: QString, mode: QString)
    joinIrcUsers(nicks: QStringList, modes: QStringList)
    part(nick: QString)
    removeChannelMode(mode: QChar, value: QString)
    removeUserMode(nick: QString, mode: QString)
    setEncrypted(encrypted: Bool)
    setPassword(password: QString)
    setTopic(topic: QString)
    setUserModes(nick: QString, modes: QString)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}
```

**Network**

```typescript
interface IrcChannel {
    ChanModes: {
        A: { [key:QChar]: QStringList },
        B: { [key:QChar]: QString },
        C: { [key:QChar]: QString },
        D: QString
    },
    UserModes: { [key:QString]: QString },
    name: QString,
    topic: QString,
    password: QString,
    encrypted: Bool
}
```

## IrcListHelper

!!! danger This part is still unfinished.

**ObjectName**

As this object is a singleton, the objectName is always ``

**Runtime**

```typescript
interface IrcListHelper {


    // C->S calls

    requestChannelList(netId: NetworkId, channelFilters: QStringList)


    // S->C calls

    receiveChannelList(netId: NetworkId, channelFilters: QStringList, channels: QVariantList)
    reportError(error: QString)
    reportFinishedList(netId: NetworkId)
}
```

**Network**

Its network representation is an empty `QVariantMap`, as it has no syncable fields. For the same reason it also does
**not** support the otherwise standard `update` call.

## IrcUser

**ObjectName**

The objectName of an IrcUser is the string representation of the networkId of the network it belongs to, followed by a
"/", followed by the nick of the user.

Example: `4/justJanne`

**Runtime**

```typescript
interface IrcUser {
    user: QString,
    host: QString,
    nick: QString,
    realName: QString,
    account: QString,
    away: Bool,
    awayMessage: QString,
    idleTime: QDateTime,
    loginTime: QDateTime,
    server: QString,
    ircOperator: QString,
    lastAwayMessage: Int,
    lastAwayMessageTime: QDateTime,
    whoisServiceReply: QString,
    suserHost: QString,
    encrypted: Bool,
    channels: QStringList,
    userModes: QString


    // S->C calls

    addUserModes(modes: QString)
    joinChannel(channelname: QString)
    partChannel(channelname: QString)
    quit()
    removeUserModes(modes: QString)
    setAccount(account: QString)
    setAway(away: Boolean)
    setAwayMessage(awayMessage: QString)
    setEncrypted(encrypted: Boolean)
    setHost(host: QString)
    setIdleTime(idleTime: QDateTime)
    setIrcOperator(ircOperator: QString)
    setLastAwayMessage(lastAwayMessage: Int)
    setLastAwayMessageTime(lastAwayMessageTime: QDateTime)
    setLoginTime(loginTime: Temporal)
    setNick(nick: QString)
    setRealName(realName: QString)
    setServer(server: QString)
    setSuserHost(suserHost: QString)
    setUser(user: QString)
    setUserModes(modes: QString)
    setWhoisServiceReply(whoisServiceReply: QString)
    updateHostmask(mask: QString)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}
```

**Network**

```typescript
interface IrcUser {
    user: QString,
    host: QString,
    nick: QString,
    realName: QString,
    account: QString,
    away: Bool,
    awayMessage: QString,
    idleTime: QDateTime,
    loginTime: QDateTime,
    server: QString,
    ircOperator: QString,
    lastAwayMessage: Int,
    lastAwayMessageTime: QDateTime,
    whoisServiceReply: QString,
    suserHost: QString,
    encrypted: Bool,
    channels: QStringList,
    userModes: QString
}
```

## Network

**ObjectName**

The objectName of a Network is the string representation of the networkId.

Example: `4`

**Runtime**

```typescript
interface Network {
    myNick: QString,
    latency: Int,
    currentServer: QString,
    isConnected: Bool,
    connectionState: ConnectionState,
    prefixes: QChar[],
    prefixModes: QChar[],
    channelModes: { [key:ChannelModeType]: QChar[] },
    ircUsers: { [key:QString]: IrcUser },
    ircChannels: { [key:QString]: IrcChannel },
    supports: { [key:QString]: QString },
    caps: { [key:QString]: QString },
    capsEnabled: QString[],
    networkInfo: NetworkInfo


    // C->S calls

    requestConnect()
    requestDisconnect()
    requestSetNetworkInfo(info: NetworkInfo)


    // S->C calls

    acknowledgeCap(capability: QString)
    addCap(capability: String, value: QString)
    addIrcChannel(channel: QString)
    addIrcUser(hostmask: QString)
    addSupport(key: QString, value: QString)
    clearCaps()
    emitConnectionError(error: QString)
    ircUserNickChanged(before: QString, after: QString)
    removeCap(capability: QString)
    removeSupport(key: QString)
    setAutoIdentifyPassword(password: QString)
    setAutoIdentifyService(service: QString)
    setAutoReconnectInterval(interval: UInt)
    setAutoReconnectRetries(retries: UShort)
    setCodecForDecoding(codecName: QByteBuffer | null)
    setCodecForEncoding(codecName: QByteBuffer | null)
    setCodecForServer(codecName: QByteBuffer | null)
    setConnected(isConnected: Bool)
    setConnectionState(state: Int)
    setCurrentServer(currentServer: QString)
    setIdentity(identity: IdentityId)
    setLatency(latency: Int)
    setMessageRateBurstSize(burstSize: UInt)
    setMessageRateDelay(messageDelay: UInt)
    setMyNick(mynick: QString)
    setNetworkName(networkName: QString)
    setNetworkInfo(info: NetworkInfo)
    setPerform(perform: QStringList)
    setRejoinChannels(rejoinChannels: Bool)
    setSaslAccount(account: QString)
    setSaslPassword(password: QString)
    setServerList(serverList: QVariantList)
    setActualServerList(serverList: NetworkServer[])
    setUnlimitedMessageRate(unlimitedRate: Bool)
    setUnlimitedReconnectRetries(unlimitedRetries: Bool)
    setUseAutoIdentify(autoIdentify: Bool)
    setUseAutoReconnect(autoReconnect: Bool)
    setUseCustomMessageRate(useCustomRate: Bool)
    setUseRandomServer(randomServer: Bool)
    setUseSasl(sasl: Bool)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}

interface NetworkServer {
    host: QString,
    port: UInt,
    password: QString,
    useSSL: Bool,
    sslVerify: Bool,
    sslVersion: Int,
    useProxy: Bool,
    proxyType: Int,
    proxyHost: QString,
    proxyPort: UInt,
    proxyUser: QString,
    proxyPass: QString
}

enum ConnectionState {
    Disconnected = 0x00,
    Connecting = 0x01,
    Initializing = 0x02,
    Initialized = 0x03,
    Reconnecting = 0x04,
    Disconnecting = 0x05
}

enum ChannelModeType {
    NOT_A_CHANMODE = 0x00,
    A_CHANMODE = 0x01,
    B_CHANMODE = 0x02,
    C_CHANMODE = 0x04,
    D_CHANMODE = 0x08
}
```

**Network**

```typescript
interface Network {
    Caps: { [key:QString]: QString },
    CapsEnabled: QString[],
    IrcUsersAndChannels: {
        Users: {
            user: QString[],
            host: QString[],
            nick: QString[],
            realName: QString[],
            account: QString[],
            away: Bool[],
            awayMessage: QString[],
            idleTime: QDateTime[],
            loginTime: QDateTime[],
            server: QString[],
            ircOperator: QString[],
            lastAwayMessage: Int[],
            lastAwayMessageTime: QDateTime[],
            whoisServiceReply: QString[],
            suserHost: QString[],
            encrypted: Bool[],
            channels: QStringList[],
            userModes: QString[]
        },
        Channels: {
            ChanModes: {
                A: { [key:QString]: QStringList },
                B: { [key:QString]: QString },
                C: { [key:QString]: QString },
                D: QString
            }[],
            UserModes: { [key:QString]: QString }[],
            name: QString[],
            topic: QString[],
            password: QString[],
            encrypted: Bool[]
        }
    },
    ServerList: {
        Host: QString,
        Port: UInt,
        Password: QString,
        UseSSL: Bool,
        sslVerify: Bool,
        sslVersion: Int,
        UseProxy: Bool,
        ProxyType: Int,
        ProxyHost: QString,
        ProxyPort: UInt,
        ProxyUser: QString,
        ProxyPass: QString
    }[],
    Supports: { [key:QString]: QString },
    networkId: NetworkId,
    networkName: QString,
    currentServer: QString,
    myNick: QString,
    latency: Int,
    codecForServer: QByteArray
    codecForEncoding: QByteArray
    codecForDecoding: QByteArray
    identityId: IdentityId,
    isConnected: Bool,
    connectionState: Int,
    useRandomServer: Bool,
    perform: QStringList,
    useAutoIdentify: Bool,
    autoIdentifyService: QString,
    autoIdentifyPassword: QString,
    useSasl: Bool,
    saslAccount: QString,
    saslPassword: QString,
    useAutoReconnect: Bool,
    autoReconnectInterval: UInt,
    autoReconnectRetries: UShort,
    unlimitedReconnectRetries: Bool,
    rejoinChannels: Bool,
    useCustomMessageRate: Bool,
    msgRateBurstSize: UInt,
    msgRateMessageDelay: UInt,
    unlimitedMessageRate: Bool
}
```

## NetworkInfo

**ObjectName**

As this object is a singleton, the objectName is always `GlobalNetworkConfig`

**Runtime**

```typescript
interface NetworkInfo {
    networkName: QString,

    serverList: NetworkServer[],
    perform: QStringList,

    autoIdentifyService: QString,
    autoIdentifyPassword: QString,

    saslAccount: QString,
    saslPassword: QString,

    codecForServer: QByteArray,
    codecForEncoding: QByteArray,
    codecForDecoding: QByteArray,

    networkId: NetworkId,
    identityId: IdentityId,

    msgRateBurstSize: UInt,
    msgRateMessageDelay: UInt,

    autoReconnectInterval: UInt,
    autoReconnectRetries: UShort,

    rejoinChannels: Bool,
    useRandomServer: Bool,
    useAutoIdentify: Bool,
    useSasl: Bool,
    useAutoReconnect: Bool,
    unlimitedReconnectRetries: Bool,
    useCustomMessageRate: Bool,
    unlimitedMessageRate: Bool,
    autoAwayActive: Bool
}
```

**Network**

```typescript
interface NetworkInfo {
    NetworkName: QString,

    ServerList: NetworkServer[],
    Perform: QStringList,

    AutoIdentifyService: QString,
    AutoIdentifyPassword: QString,

    SaslAccount: QString,
    SaslPassword: QString,

    CodecForServer: QByteArray,
    CodecForEncoding: QByteArray,
    CodecForDecoding: QByteArray,

    NetworkId: NetworkId,
    IdentityId: IdentityId,

    MsgRateBurstSize: UInt,
    MsgRateMessageDelay: UInt,

    AutoReconnectInterval: UInt,
    AutoReconnectRetries: UShort,

    RejoinChannels: Bool,
    UseRandomServer: Bool,
    UseAutoIdentify: Bool,
    UseSasl: Bool,
    UseAutoReconnect: Bool,
    UnlimitedReconnectRetries: Bool,
    UseCustomMessageRate: Bool,
    UnlimitedMessageRate: Bool,
    AutoAwayActive: Bool
}
```

## NetworkConfig

**Runtime**

```typescript
interface NetworkConfig {
    pingTimeoutEnabled: Bool,
    pingInterval: Int,
    maxPingCount: Int,
    autoWhoEnabled: Bool,
    autoWhoInterval: Int,
    autoWhoNickLimit: Int,
    autoWhoDelay: Int,
    standardCtcp: Bool


    // C->S calls

    requestSetAutoWhoDelay(delay: Int)
    requestSetAutoWhoEnabled(enabled: Boolean)
    requestSetAutoWhoInterval(interval: Int)
    requestSetAutoWhoNickLimit(limit: Int)
    requestSetMaxPingCount(count: Int)
    requestSetPingInterval(interval: Int)
    requestSetPingTimeoutEnabled(enabled: Boolean)
    requestSetStandardCtcp(enabled: Boolean)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    requestUpdate(properties: QVariantMap)


    // S->C calls

    setAutoWhoDelay(delay: Int)
    setAutoWhoEnabled(enabled: Boolean)
    setAutoWhoInterval(interval: Int)
    setAutoWhoNickLimit(limit: Int)
    setMaxPingCount(count: Int)
    setPingInterval(interval: Int)
    setPingTimeoutEnabled(enabled: Boolean)
    setStandardCtcp(standardCtcp: Boolean)
    /**
     * Replaces all properties of the object with the content of the
     * "properties" parameter. This parameter is in network representation.
     */
    update(properties: QVariantMap)
}
```

**Network**

```typescript
interface NetworkConfig {
    pingTimeoutEnabled: Bool,
    pingInterval: Int,
    maxPingCount: Int,
    autoWhoEnabled: Bool,
    autoWhoInterval: Int,
    autoWhoNickLimit: Int,
    autoWhoDelay: Int,
    standardCtcp: Bool
}
```

[AoS to SoA]: #aos-to-soa
[QVariantMap to QVariantList]: #qvariantmap-to-qvariantlist
