---
title: RPC Calls
description: Developer documentation for RPC calls of the Quassel protocol
---

For RPC calls, the full name – e.g. "2createIdentity(Identity,QVariantMap)" has to be transmitted, including the number
at the beginning and the arglist.

## C->S calls

### `2createIdentity(Identity,QVariantMap)`

- `identity`: `Identity`
- `additional`: `QVariantMap`  
  Always empty

### `2removeIdentity(IdentityId)`

- `identity`: `IdentityId`

### `2createNetwork(NetworkInfo,QStringList)`

- `networkInfo`: `NetworkInfo`
- `channels`: `QStringList`

### `2removeNetwork(NetworkId)`

- `network`: `NetworkId`

### `2changePassword(PeerPtr,QString,QString,QString)`

- `peerPtr`: `Long`  
  Always zero, only has a value within of the core itself.
- `user`: `QString`
  Username
- `before`: `QString`
  Old password
- `after`: `QString`
  New password

### `2kickClient(int)`

- `id`: `Int`  
  Client id of client to be kicked.  
  Ids can be found here [CoreInfo]

### `2sendInput(BufferInfo,QString)`

- `bufferInfo`: `BufferInfo`
- `message`: `QString`

## S->C calls

### `__objectRenamed__`

- `classname`: `QByteArray`
- `newname`: `QString`
- `oldname`: `QString`

Called whenever an object has been renamed, and the object store should update its name. All future sync calls for this
object will use the new name instead.

### `2displayMsg(Message)`

- `message`: `Message`

Called when a new IRC message has been received, and the client should display or store it.

### `2displayStatusMsg(QString,QString)`

- `network`: `QString`
- `message`: `QString`

Status message for an IRC network to be shown in the client’s status bar (if available).

### `2bufferInfoUpdated(BufferInfo)`

- `bufferInfo`: `BufferInfo`

### `2identityCreated(Identity)`

- `identity`: `Identity`

### `2identityRemoved(IdentityId)`

- `identity`: `IdentityId`

### `2networkCreated(NetworkId)`

- `network`: `NetworkId`

### `2networkRemoved(NetworkId)`

- `network`: `NetworkId`

### `2passwordChanged(PeerPtr,bool)`

- `peerPtr`: `Long`  
  Always zero, only has a value within of the core itself
- `success`: `Bool`

Returns if the recent password change attempt has been a success.

This is one of the few responses which only gets sent to the client which sent the original request.

### `2disconnectFromCore()`

Requests the current client to disconnect from the core. Only this client sees this message.

[CoreInfo]: objects.md#coreinfo
