---
title: FAQ
description: Frequently asked Questions regarding Quasseldroid
---

## What is a quassel core? Why do I need one to use Quasseldroid?

A quassel core is a server which connects to IRC servers, and ensures you are always online — storing logs of the
messages in the channel, etc.

Quasseldroid connects to the core instead of connecting directly to the IRC server, so if you turn your phone off, or
your phone’s internet connection drops, your unread messages are still saved, and Quasseldroid can show them whenever
your phone turns on again.

Without the quassel core, you’d miss any messages on IRC that were sent while you weren’t using Quasseldroid.

## Where is autocomplete?

Quasseldroid supports many options for nick autocompletion:

1. **Prefix Autocomplete**  
  ![New Autocomplete Example With Prefix](../images/autocomplete_new_prefix.png#half-width)  
  When typing an @ or # with the name of a nick or channel afterwards, Quasseldroid automatically recommends You can
  also configure this to always autocomplete even without prefix

2. **Double-Tap to Autocomplete**  
  ![New Autocomplete Example With Doubletap](../images/autocomplete_new_doubletap.png#half-width)  
  When typing a name, you can double-tap the input box to autocomplete the name

3. **Autocomplete Button**  
  ![New Autocomplete Example With Button](../images/autocomplete_new_button.png#half-width)  
  Of course, the good old autocomplete button can be re-enabled again, too.
