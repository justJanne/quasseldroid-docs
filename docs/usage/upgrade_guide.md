---
title: Upgrade Guide
description: Upgrade Guide for users of old versions of Quasseldroid
---

This guide is intended to help users upgrading from older versions with the recent changes.

## Autocomplete

In the old versions, autocomplete was only possible through the use of the autocomplete button in the input line.

![Old Autocomplete Example](../images/autocomplete_old.png#half-width)

Nowadays, there are many possible solutions for autocomplete:

1. **Prefix Autocomplete**  
  ![New Autocomplete Example With Prefix](../images/autocomplete_new_prefix.png#half-width)  
  When typing an @ or # with the name of a nick or channel afterwards, Quasseldroid automatically recommends a name.
  You can also configure this to always autocomplete even without prefix

2. **Double-Tap to Autocomplete**  
  ![New Autocomplete Example With Doubletap](../images/autocomplete_new_doubletap.png#half-width)  
  When typing a name, you can double-tap the input box to autocomplete the name.

3. **Autocomplete Button**  
  ![New Autocomplete Example With Button](../images/autocomplete_new_button.png#half-width)  
  Of course, the good old autocomplete button can be re-enabled again, too.

## Avatars

Quasseldroid now display sender icons in messages, chat lists, the header, and notifications.

You can enable Avatar fallbacks to load avatars for users if they are using Matrix or IRCCloud, or have a gravatar email
in their realname field.

Avatars can be configured to be square or round, and additionally, you can disable the avatars in the message list as
well.

## Chat Layout

The new default chat layout shows nicks in a separate row, groups messages from the same user, and shows avatars.

To return to the previous layout, you can disable "Show Avatars", "Separate Nicknames" and "Right-Aligned Timestamps".

## Highlights & Unread Activity

In Quassel 0.13, highlights and unread activity are managed by the core. This means you won’t ever miss a highlight.
Additionally, your client only has to load unread messages of the channels you actually open, saving traffic and
improving performance.

Quasseldroid relies on this functionality for showing highlights and unread activity, as well as configuring highlights.
Accordingly, a Quassel 0.13 Core is now required for this functionality to work.
