# Project information
site_name: 'Quasseldroid'
site_description: 'Chat comfortably, everywhere.'
site_author: 'Janne Mareike Koschinski'
site_url: 'https://quasseldroid.info/docs/'
repo_url: 'https://git.kuschku.de/justjanne/quasseldroid-ng'
repo_name: 'justjanne/quasseldroid-ng'
edit_uri: ""
copyright: 'Copyright &copy; 2022 Janne Mareike Koschinski'

nav:
  - index.md
  - "Usage":
      - usage/faq.md
      - usage/upgrade_guide.md
  - "Protocol":
      - protocol/overview.md
      - protocol/serialization.md
      - "Layers":
          - protocol/layers/framing.md
          - protocol/layers/message.md
      - "Handshake":
          - protocol/handshake/features.md
          - protocol/handshake/messages.md
      - "SignalProxy":
          - protocol/signalproxy/messages.md
          - protocol/signalproxy/objects.md
          - protocol/signalproxy/rpc.md

theme:
  name: 'material'
  custom_dir: 'overrides'
  font: false
  palette:
    - media: "(prefers-color-scheme: light)"
      scheme: default
      toggle:
        icon: material/weather-night
        name: Switch to dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-7
        name: Switch to light mode
  logo: 'assets/logo.svg'
  favicon: 'favicon.png'
  icon:
    repo: 'fontawesome/brands/git-alt'
  features:
    - navigation.sections
    - navigation.instant
    - navigation.expand
    - search.highlight
    - toc.integrate

extra:
  homepage: 'https://quasseldroid.info'
  social:
    - icon: 'fontawesome/brands/twitter'
      link: 'https://twitter.com/quasseldroid'
    - icon: 'fontawesome/solid/earth-americas'
      link: 'https://quasseldroid.info/'
    - icon: 'fontawesome/brands/git-alt'
      link: 'https://git.kuschku.de/justjanne/quasseldroid-ng'

plugins:
  - search
  - git-revision-date

extra_javascript:
  - assets/source_info.js

extra_css:
  - assets/palette.css
  - assets/application.css
  - assets/roboto.css
  - assets/fonts.css

markdown_extensions:
  - admonition
  - footnotes
  - meta
  - toc:
      permalink: true
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.highlight:
      use_pygments: true
      linenums: true
      anchor_linenums: true
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.superfences
  - pymdownx.tilde
